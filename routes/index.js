
var express = require('express'),
    router = express.Router(),
    getUserBySession = require('../lib/getUserBySession'),
    models = require('../models');

router.get('/', getUserBySession, function(req, res) {
  res.render('index', { title: 'Express' });
});

router.use('/account', require('./account'));

router.get('/users', function(req, res) {
  var options = {};
  if(req.session && req.session.user && req.session.user.id) {
    options = {exclude_id: req.session.user.id};
  }
  models.User.textSearch(req.query.q, options, function(err, foundUsers) {
    if(err) {
      return res.status(500).send({status: 500, err: err});
    }

    res.send({status: 200, users: foundUsers});
  });
});


module.exports = router;
