
var models = require('../../models'),
    debug = require('../../lib/debug')('socket:messages');

module.exports = function(socket/*, io*/) {
  socket.on('messages.markAsRead', function(messages_ids, dialog_id, cb) {
    models.Dialog.markMessagesAsRead(messages_ids, dialog_id, socket.session.user.id, function(err, updatedDialog) {
      if(err) {
        debug(err);
        return cb(err);
      }

      cb(null, updatedDialog);
    });
  });
};
