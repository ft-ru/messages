
var _ = require('lodash'),
    models = require('../../models'),
    config = require('../../lib/config'),
    io = require('socket.io').listen(config.get('socketPORT')),
    getSessionFrom = require('../../lib/getSessionFrom'),
    dialogs = require('./dialogs'),
    messages = require('./messages');

//io.adapter(require('socket.io-redis')('localhost:6379'));

var authBySocketHandshake = function(handshake, cb) {
  if(handshake.query.session) {
    // check session
    getSessionFrom.session(handshake.query.session, cb);
  } else {
    // parse cookies, check session
    getSessionFrom.cookies(handshake.headers.cookie, cb);
  }
};

io.use(function(socket, next) {
  authBySocketHandshake(socket.handshake, function(err, session) {
    if(err) {
      return next(err);
    }
    if(!session || !session.user) {
      return next(new Error('Authentication error'));
    }

    models.User.findById(session.user.id).exec(function(err, foundUser) {
      if(err) {
        throw err;
      }

      foundUser.online = true;
      foundUser.save(function(err) {
        if(err) {
          throw err;
        }

        socket.session = {
          user: session.user
        };
        next();
      });
    });
  });
});

io.on('connection', function(socket) {
  console.log('connection');
  socket.emit('auth', socket.session);

  dialogs(socket, io);
  messages(socket, io);

  socket.on('disconnect', function(/*reason*/) {
    models.User.findById(socket.session.user.id).exec(function(err, foundUser) {
      if(err) {
        throw err;
      }

      foundUser.online = false;
      foundUser.lastSeen = Date.now();
      foundUser.save(function(err) {
        if(err) {
          throw err;
        }

        console.log('disconnect');
      });
    });
  });

  //setInterval(function() {
  //  console.log(socket.rooms);
  //}, 10000);
  //setInterval(function() {
  //  console.log(io.nsps['/'].adapter.rooms);
  //}, 5000);
  //console.log(io.nsps['/'].adapter.rooms);
  //console.log(socket.session.user);
  //setInterval(function() {
  //  console.log(socket.rooms.filter(function(room) {return room != socket.id;}));
  //}, 5000);



});

module.exports = io;
