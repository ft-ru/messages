
// "create", "read", "update", or "delete"

var models = require('../../models'),
    _ = require('lodash'),
    debug = require('../../lib/debug')('socket:dialogs');

var calculateUnreadForUser = function(user_id, dialogJSON) {
  dialogJSON.unreadCounters = dialogJSON.unreadCounters.map(function(unread) {return unread.toJSON()});
  dialogJSON.unread = _.find(dialogJSON.unreadCounters, {user_id: user_id}).count;
  return dialogJSON;
};

module.exports = function(socket, io) {
  socket.on('dialogs:read', function(cb) {
    models.Dialog.getUserDialogs(socket.session.user.id, function(err, foundDialogs) {
      if(foundDialogs && foundDialogs.length) {
        foundDialogs.forEach(function(dialog, index, all) {
          all[index] = calculateUnreadForUser(socket.session.user.id, dialog.toJSON(/*{virtuals: true, getters: true, transform: true}*/));
          socket.join(dialog.id);
        });
      }

      //debug('dialogs:read', foundDialogs);
      if(cb && typeof cb === 'function') {
        cb(err, foundDialogs);
      }
    });
  });

  socket.on('dialogs:create', function(data, cb) {
    debug('dialogs:create', arguments);
    models.Dialog.createNewDialog([socket.session.user.id, data.user_id], function(err, createdDialog) {
      if(err) {
        return cb(err);
      }
      socket.join(createdDialog.id);
      var otherUserSocket = _.find(io.sockets.sockets, function(socket) {
        return socket.session.user.id == data.user_id;
      });
      if(otherUserSocket) {
        otherUserSocket.emit('dialogs:create', createdDialog);
      }
      cb(null, createdDialog);
    });
  });

  socket.on('dialog:read', function(dialog_id, cb) {
    debug('dialog:read', arguments);
    models.Dialog.getDialog(dialog_id, function(err, foundDialog) {
      if(err) {
        return cb(err);
      }
      socket.join(foundDialog.id);
      cb(null, foundDialog);
    });
  });

  socket.on('dialog.addUser', function(data, cb) {
    debug('dialog.addUser', arguments);
    models.Dialog.addUserToDialog(data.user_id, data.dialog_id, function(err, updatedDialog, serviceMessage) {
      if(err) {
        return cb(err);
      }

      var otherUserSocket = _.find(io.sockets.sockets, function(socket) {
        return socket.session.user.id == data.user_id;
      });
      if(otherUserSocket) {
        otherUserSocket.emit('dialogs:create', updatedDialog);
      }
      io.to(data.dialog_id).emit('dialogs.newMessage', {dialog_id: data.dialog_id, message: serviceMessage});
      cb(null, updatedDialog);
    });
  });

  socket.on('dialogs.newMessage', function(data, cb) {
    var newMessageData = {
      from: socket.session.user.id,
      text: data.text,
      attachments: data.attachments
    };
    models.Dialog.newMessage(data.dialog_id, newMessageData, function(err, addedMessage, updatedDialog) {
      if(err) {
        return cb(err);
      }

      io.to(data.dialog_id).emit('dialogs.newMessage', {dialog_id: data.dialog_id, message: addedMessage});
      cb(null, addedMessage, updatedDialog);
    });
  });

};
