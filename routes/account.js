
var express = require('express'),
    router = express.Router(),
    _ = require('lodash'),
    models = require('../models');

router.get('/logout', function(req, res, next) {
  req.session.destroy(function(err) {
    if(err) {
      return next(err);
    }

    res.redirect('/');
  });
});

router.post('/auth', function(req, res) {
  models.User.getOrCreateUser(req.body, function(err, foundUser) {
    if(err) {
      return res.status(500).send({status: 500, err: err});
    }

    req.session.user = _.pick(foundUser, ['id', 'name', 'surname', 'email']);
    res.send({status: 200, user: _.pick(foundUser, ['id', 'name', 'surname', 'email'])});
  });
});



module.exports = router;

