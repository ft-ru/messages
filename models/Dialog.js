
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    _ = require('lodash'),
    Q = require('q'),
    debug = require('../lib/debug')('models:dialog'),
    DialogModel;

var attachmentsSchema = new Schema({
  url: String,
  name: String
});
attachmentsSchema.options.toJSON = {
  transform: function(doc, ret) {
    ret = _.pick(doc, ['id', 'url', 'name']);
    return ret;
  }
};

var messagesSchemaObject = {
  from: {type: Schema.Types.ObjectId, ref: 'User'},
  text: String,
  createdAt: {type: Date, default: Date.now},
  status: {type: Number, default: 0}, // 0 - not read, 1 - read
  messageType: {type: String, enum: ['message', 'service'], default: 'message'},
  attachments: [attachmentsSchema]
};
var messagesSchema = new Schema(messagesSchemaObject);

messagesSchema.options.toJSON = {
  transform: function(doc, ret) {
    if(ret.url) {
      ret = ret.toJSON();
      return ret;
    }
    ret = _.pick(doc, ['id', 'from', 'text', 'createdAt', 'status', 'attachments', 'messageType']);
    return ret;
  }
};

var messageModel = mongoose.model('Message', messagesSchema);

var unreadCountersSchema = new Schema({
  user_id: {type: Schema.Types.ObjectId, ref: 'User'},
  count: {type: Number, default: 0}
});
unreadCountersSchema.options.toJSON = {
  transform: function (doc, ret) {
    ret = _.pick(doc, ['user_id', 'count']);
    ret.user_id = ret.user_id.toString();
    return ret;
  }
};

var dialogsSchema = new Schema({
  users: [
    {type: Schema.Types.ObjectId, ref: 'User'}
  ],
  messages: [messagesSchema],
  title: {type: String},
  lastMessage: messagesSchemaObject,
  unreadCounters: [unreadCountersSchema]
});

dialogsSchema.options.toJSON = {
  transform: function(doc, ret) {
    if('function' == typeof doc.ownerDocument) {
      ret = ret.toJSON();
      return ret;
    }
    ret = _.pick(doc, ['id', 'users', 'messages', 'title', 'lastMessage', 'unreadCounters']);
    return ret;
  }
};


/**
 * getUserDialogs
 * @param {String|ObjectId} user_id
 * @param {Function} cb
 */
dialogsSchema.statics.getUserDialogs = function(user_id, cb) {
  this.find({users: user_id}).select('-messages').populate('users').exec(cb);
};

/**
 * getDialog
 * @param {String|ObjectId} dialog_id
 * @param {Function} cb
 */
dialogsSchema.statics.getDialog = function(dialog_id, cb) {
  this.findById(dialog_id).populate('users').exec(cb);
};

dialogsSchema.statics.getUserDialogsUnreadCount = function(user, cb) {
  // TODO: move to mongoDB
  this.find({'unreadCounters.user_id': user.id}).select('unreadCounters').exec(function(err, foundDialogs) {
    if(err) {
      return cb(err);
    }

    var count = _.sum(
        _(foundDialogs)
            .pluck('unreadCounters')
            .flatten()
            .filter(function(counter) {
              return counter.user_id == user.id;
            })
            .pluck('count')
            .value()
    );

    cb(null, count);
  });
};

dialogsSchema.statics.getUserDialogUnreadMessagesCount = function(user, dialog, cb) {
  this.aggregate(
      {$match: {_id: dialog.id, 'messages.from': {$ne: user.id}, status: 0}},
      {$group: {_id: null, count: {$sum: 1}}}
  ).exec(function(err, found) {
    console.log(err, found);
    cb(err, found);
  });
};

/**
 * _checkDialogExisting
 * @param {Array} user_ids
 * @param {Function} cb
 * @private
 */
dialogsSchema.statics._checkDialogExisting = function(user_ids, cb) {
  this.find({users: {$all: user_ids}}).select('-messages').populate('users').exec(function(err, foundDialogs) {
    if(err) {
      return cb(err);
    }

    if(!foundDialogs.length) {
      cb(null, null);
    } else {
      var existedDialog = null;
      foundDialogs.forEach(function(dialog) {
        if(dialog.users.length == user_ids.length) {
          existedDialog = dialog;
        }
      });
      cb(null, existedDialog);
    }
  });
};

/**
 * createNewDialog
 * @param {Array} user_ids
 * @param {Function} cb
 */
dialogsSchema.statics.createNewDialog = function(user_ids, cb) {
  var model = this;
  this._checkDialogExisting(user_ids, function(err, existedDialog) {
    if(err) {
      return cb(err);
    }

    if(existedDialog) {
      return cb(null, existedDialog);
    }

    var creatingMessage = {
      text: 'Диалог создан',
      status: 1,
      messageType: 'service'
    };

    new DialogModel({
      users: user_ids,
      unreadCounters: user_ids.map(function(id) {return {user_id: id};})
    }).save(function(err, savedDialog) {
      if(err) {
        return cb(err);
      }

      model.newMessage(savedDialog.id, creatingMessage, function(err) {
        if(err) {
          return cb(err);
        }

        model.findById(savedDialog.id).populate('users').exec(function(err, foundDialog) {
          if(err) {
            return cb(err);
          }

          cb(null, foundDialog);
        });
      });
    });
  });
};

/**
 * addUserToDialog
 * @param {String|ObjectId} user_id
 * @param {String|ObjectId} dialog_id
 * @param {Function} cb
 */
dialogsSchema.statics.addUserToDialog = function(user_id, dialog_id, cb) {
  var model = this;
  this.findByIdAndUpdate(dialog_id, {$addToSet: {users: user_id}, $push: {unreadCounters: {user_id: user_id}}}).exec(function(err, updatedDialog) {
    if(err) {
      return cb(err);
    }

    mongoose.model('User').findById(user_id).exec(function(err, foundUser) {
      if(err) {
        return cb(err);
      }

      var serviceMessage = {
        text: 'Пользователь ' + foundUser.fullName + ' добавлен в диалог',
        status: 1,
        messageType: 'service'
      };

      model.newMessage(dialog_id, serviceMessage, function(err, addedMessage, updatedDialog) {
        if(err) {
          return cb(err);
        }

        cb(null, updatedDialog, addedMessage);
      });
    });
  });
};

dialogsSchema.statics.updateDialog = function(dialog_id, cb) {};

/**
 * incrementUnreadCounter
 * @private
 * @param {String|ObjectId} user_id
 * @param {String|ObjectId} dialog_id
 * @returns {Promise}
 */
dialogsSchema.statics._incrementUnreadCounter = function(user_id, dialog_id) {
  var defer = Q.defer();
  this.findOneAndUpdate({
    _id: dialog_id,
    'unreadCounters.user_id': user_id
  }, {
    $inc: {
      'unreadCounters.$.count': 1
    }
  }).exec(function(err, updatedDialog) {
    if(err) {
      return defer.reject(err);
    }

    defer.resolve(updatedDialog);
  });
  return defer.promise;
};

/**
 * newMessage
 * @param {String|ObjectId} dialog_id
 * @param {Object} message
 * @param {Function} cb
 */
dialogsSchema.statics.newMessage = function(dialog_id, message, cb) {
  var newMessage = new messageModel(message),
      model = this,
      lastMessage = newMessage;

  if(message.messageType == 'service') {
    lastMessage = message;
  }

  model.findByIdAndUpdate(dialog_id, {$push: {messages: newMessage}, $set: {lastMessage: lastMessage}}).exec(function(err, updatedDialog) {
    if(err) {
      return cb(err);
    }

    var addedMessage = updatedDialog.messages.pop();
    var userIds = _.pluck(updatedDialog.unreadCounters, 'user_id').filter(function(user_id) {return user_id != message.from;});

    Q.all(userIds.reduce(function(promise, user_id) {
      return promise.then(function() {
        return model._incrementUnreadCounter(user_id, dialog_id);
      });
    }, Q())).nodeify(function(err, updatedDialog) {
      if(err) {
        return cb(err);
      }
      cb(null, addedMessage, updatedDialog);
    });
  });
};

/**
 * markMessage
 * @private
 * @param {String|ObjectId} message_id
 * @param {String|ObjectId} dialog_id
 * @param {String|ObjectId} user_id
 * @returns {Promise}
 */
dialogsSchema.statics._markMessageAsRead = function(message_id, dialog_id, user_id) {
  var defer = Q.defer(),
      model = this;

  model.findOneAndUpdate({_id: dialog_id, 'messages._id': message_id}, {$set: {'messages.$.status': 1}}).exec(function(err/*, messageStatusUpdated*/) {
    if(err) {
      debug(err);
      return defer.reject(err);
    }

    debug('_markMessageAsRead: message status has been set to 1');
    model.findOneAndUpdate({
      _id: dialog_id,
      unreadCounters: {
        $elemMatch: {
          user_id: user_id,
          count: {$gt: 0}
        }
      }
    }, {
      $inc: {
        'unreadCounters.$.count': -1
      }
    }).exec(function(err, unreadCounterUpdated) {
      if(err) {
        debug(err);
        return defer.reject(err);
      }

      debug('_markMessageAsRead: unread counter for user', user_id, 'has been decreased');
      defer.resolve(unreadCounterUpdated);
    });
  });
  return defer.promise;
};

/**
 * markMessagesAsRead
 * @param {Array} messages_ids
 * @param {String|ObjectId} dialog_id
 * @param {String|ObjectId} user_id
 * @param {Function} cb
 */
dialogsSchema.statics.markMessagesAsRead = function(messages_ids, dialog_id, user_id, cb) {
  var model = this;

  debug('markMessagesAsRead: messages:', messages_ids);
  debug('markMessagesAsRead: dialog:', dialog_id);

  messages_ids.reduce(function(promise, message_id) {
    return promise.then(function() {
      return model._markMessageAsRead(message_id, dialog_id, user_id);
    });
  }, Q()).nodeify(cb);
};

module.exports = DialogModel = mongoose.model('Dialog', dialogsSchema);
