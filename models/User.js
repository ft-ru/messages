var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    _ = require('lodash'),
    dialogModel = mongoose.model('Dialog');

var usersSchema = new Schema({
  name: {type: String, required: true},
  surname: {type: String},
  phone: {type: String},
  email: {type: String},
  about: {type: String},
  photo: {
    original: String,
    preview: String
  },
  social: {
    socialId: String,
    socialNetwork: String,
    identity: {type: String, unique: true}
  },
  rawSocialData: Schema.Types.Mixed,
  online: {type: Boolean, default: false},
  lastSeen: {type: Date, default: Date.now}
});

usersSchema.options.toJSON = {
  transform: function(doc, ret) {
    ret = _.pick(doc, ['id', 'name', 'surname', 'phone', 'email', 'about', 'photo', 'fullName', 'online', 'lastSeen']);
    return ret;
  }
};

usersSchema.virtual('fullName').get(function() {
  return _.compact([this.name, this.surname]).join(' ');
}).set(function(fullName) {
  var names = fullName.split(' ');
  this.name = names[0];
  this.surname = names[1];
});

/**
 * textSearch
 * @param {String} text
 * @param {Object} [options]
 * @param {Function} cb
 */
usersSchema.statics.textSearch = function(text, options, cb) {
  if(!cb && typeof options === 'function') {
    cb = options;
    options = null;
  }
  options = _.extend({
    fields: ['name', 'surname'],
    limit: 20
  }, options);

  var preparedRegExp = text.replace(/\(|\)|-|\\|\^|\$|\*|\+|\?|\{|\}|\.|\[|\]|\|/g, '\\$&'),
      query = {
        $or: options.fields.map(function(field) {
          var tmp = {};
          tmp[field] = new RegExp(preparedRegExp, 'i');
          return tmp;
        })
      };

  this.find(query).limit(options.limit).exec(function(err, foundUsers) {
    if(err) {
      return cb(err);
    }

    if(options.exclude_id) {
      foundUsers = foundUsers.filter(function(user) {return user.id != options.exclude_id});
    }
    cb(null, foundUsers);
  });
};

/**
 * _preProcessUserData
 * @private
 * @param {Object} data
 * @returns {Object}
 */
var _preProcessUserData = function(data) {
  if(data.social.socialNetwork == 'twitter') {
    data.photo.original = data.photo.original.replace('_normal.', '.');
  }
  return data;
};

/**
 * getOrCreateUser
 * @param {Object} data
 * @param {Function} cb
 */
usersSchema.statics.getOrCreateUser = function(data, cb) {
  var model = this,
      userData = _preProcessUserData(data);

  model.findOne({'social.identity': userData.social.identity}).exec(function(err, foundUser) {
    if(err) {
      return cb(err);
    }

    if(!_.isEmpty(foundUser)) {
      return cb(null, foundUser);
    }

    new model(userData).save(function(err, savedUser) {
      if(err) {
        return cb(err);
      }

      cb(null, savedUser);
    });
  });
};

usersSchema.methods.getNotificationsCount = function(cb) {
  var user = this;
  dialogModel.getUserDialogsUnreadCount(user, function(err ,count) {
    if(err) {
      return cb(err);
    }

    cb(null, count);
  });
};

module.exports = mongoose.model('User', usersSchema);
