
var server = require('../../bin/www'),
    request = require('supertest'),
    should = require('should'),
    io = require('socket.io-client'),
    Q = require('q'),
    _ = require('lodash'),
    models = require('../../models');

request(server);

var usersCollection = [
      {
        name: 'user1',
        email: 'user1@example.com',
        social: {
          identity: 'user1@example.com'
        }
      },
      {
        name: 'user2',
        email: 'user2@example.com',
        social: {
          identity: 'user2@example.com'
        }
      },
      {
        name: 'user3',
        email: 'user3@example.com',
        social: {
          identity: 'user3@example.com'
        }
      }
    ],
    createdUsers = [];

Q.longStackSupport = true;

describe('Creating few users', function() {
  before(function(done) {
    models.User.remove({email: {$in: _.pluck(usersCollection, 'email')}}).exec(function(err) {
      if(err) {
        return done(err);
      }

      //console.log(qwe);
      models.Dialog.remove({$or: [{"messages.text": "first message"}, {"messages.text": "message from user1 to user2"}]}).exec(function(err) {
        if(err) {
          return done(err);
        }

        done();
      });
    });
  });

  it('should create 3 new users', function(done) {
    var promises = [];
    usersCollection.forEach(function(user) {
      var defer = Q.defer();
      new models.User(user).save(function(err, savedUser) {
        if(err) {
          defer.reject(err);
          return done(err);
        }

        savedUser.should.have.properties(_.pick(user, ['name', 'email']));
        savedUser.should.have.properties(['online', 'lastSeen']);
        createdUsers.push(savedUser.toJSON());
        defer.resolve();
      });
      promises.push(defer.promise);
    });
    Q.all(promises).then(function() {
      done();
    });
  });
});

describe('Auth users at service', function() {
  it('should users auth at service by POST request', function(done) {
    var promises = [];
    usersCollection.forEach(function(user) {
      var defer = Q.defer();
      request(server)
          .post('/account/auth')
          .send(user)
          .expect(200)
          .end(function(err, res) {
            if(err) {
              throw err;
            }

            res.body.status.should.be.equal(200);
            should.exist(res.headers['set-cookie']);
            var cookiesKeyVal = [];

            res.headers['set-cookie'].forEach(function(cookieString) {
              var cookie = cookieString.split(';').map(_.trim)[0].split('=');
              cookiesKeyVal.push({name: cookie[0], value: cookie[1]});
            });
            user.sessionCookie = _.find(cookiesKeyVal, {name: 'connect.sid'});
            should.exist(user.sessionCookie);
            defer.resolve();
          });
      promises.push(defer.promise);
    });
    Q.all(promises).done(function() {
      done();
    }, function(err) {
      throw err;
    });
  });
});

describe('Connecting to socket.io', function() {
  it('should connect each user to socket.io server', function(done) {
    var promises = [];
    usersCollection.forEach(function(user) {
      var defer = Q.defer();
      user.socket = io.connect('http://localhost:3401?session=' + user.sessionCookie.value, {multiplex: false});

      user.socket.on('connect', function() {
        console.log("socket connected");
        defer.resolve();
      });
      promises.push(defer.promise);
    });
    Q.all(promises).done(function() {
      done();
    }, function(err) {
      throw err;
    });
  });
});

describe('Working with dialogs', function() {
  it('should get dialogs list for user1', function(done) {
    usersCollection[0].socket.emit('dialogs:read', function(err, dialogs) {
      if(err) {
        throw err;
      }

      dialogs.should.have.length(0);
      done();
    });
  });

  it('should user1 create dialog with user2', function(done) {
    usersCollection[0].socket.emit('dialogs:create', {user_id: _.find(createdUsers, {name: 'user2'}).id}, function(err, createdDialog) {
      if(err) {
        throw err;
      }

      usersCollection[0].dialogs = [createdDialog];
      usersCollection[1].dialogs = [createdDialog];
      createdDialog.should.have.properties(['id', 'unreadCounters', 'lastMessage', 'messages', 'users']);
      createdDialog.unreadCounters.should.have.length(2);
      createdDialog.users.should.have.length(2);
      createdDialog.users.forEach(function(user) {
        user.should.have.properties(JSON.parse(JSON.stringify(_.omit(_.find(createdUsers, {name: user.name}), ['online']))));
      });
      createdDialog.messages.should.have.length(1);
      createdDialog.lastMessage.should.have.properties({messageType: 'service', status: 1});
      createdDialog.messages[0].should.have.properties({messageType: 'service', status: 1});
      done();
    });
  });

  it('should user1 send message to user2', function(done) {
    usersCollection[0].socket.emit('dialogs.newMessage', {text: 'message from user1 to user2', dialog_id: usersCollection[0].dialogs[0].id}, function(err, addedMessage, updatedDialog) {
      if(err) {
        throw err;
      }

      addedMessage.should.have.properties({
        messageType: 'message',
        from: _.find(createdUsers, {name: 'user1'}).id,
        status: 0,
        text: 'message from user1 to user2'
      });
      updatedDialog.should.have.properties(['id', 'unreadCounters', 'lastMessage', 'messages', 'users']);
      updatedDialog.unreadCounters.should.have.length(2);
      updatedDialog.users.should.have.length(2);
      updatedDialog.users.should.be.eql([_.find(createdUsers, {name: 'user1'}).id, _.find(createdUsers, {name: 'user2'}).id]);
      updatedDialog.messages.should.have.length(2);
      updatedDialog.lastMessage.should.have.properties({messageType: 'message', status: 0, from: _.find(createdUsers, {name: 'user1'}).id});
      updatedDialog.messages[1].should.have.properties({messageType: 'message', status: 0});
      done();
    });
  });
});
