
var models = require('../models'),
    _ = require('lodash'),
    Q = require('q'),
    usersCollection = [
      {
        name: 'user1',
        email: 'user1@example.com',
        social: {
          identity: 'user1@example.com'
        }
      },
      {
        name: 'user2',
        email: 'user2@example.com',
        social: {
          identity: 'user2@example.com'
        }
      },
      {
        name: 'user3',
        email: 'user3@example.com',
        social: {
          identity: 'user3@example.com'
        }
      }
    ],
    createdUsers = [],
    twoUsersIds = [];

Q.longStackSupport = true;


describe('Creating few users', function() {
  before(function(done) {
    models.User.remove({email: {$in: _.pluck(usersCollection, 'email')}}).exec(function(err) {
      if(err) {
        return done(err);
      }

      models.Dialog.remove({"messages.text": "first message"}).exec(function(err) {
        if(err) {
          return done(err);
        }
        done();
      });
    });
  });
  it('should create 3 new users', function(done) {
    var promises = [];
    usersCollection.forEach(function(user) {
      var defer = Q.defer();
      new models.User(user).save(function(err, savedUser) {
        if(err) {
          defer.reject(err);
          return done(err);
        }

        savedUser.should.have.properties(_.pick(user, ['name', 'email']));
        createdUsers.push(savedUser);
        defer.resolve();
      });
      promises.push(defer.promise);
    });
    Q.all(promises).then(function() {
      done();
    });
  });
});

var newCreatedDialog;

describe('Dialogs between created users', function() {
  before(function() {
    twoUsersIds = _.sample(_.pluck(createdUsers, 'id'), 2);
  });

  it('should create dialog between 2 created users', function(done) {
    models.Dialog.createNewDialog(twoUsersIds, function(err, createdDialog) {
      if(err) {
        return done(err);
      }

      createdDialog.messages.should.have.length(1);
      createdDialog.messages[0].should.have.properties({status: 1, messageType: 'service'});
      createdDialog.unreadCounters.should.have.length(2);
      createdDialog.unreadCounters.forEach(function(unread) {
        unread.count.should.equal(1);
      });
      createdDialog.users.should.have.length(2);
      newCreatedDialog = createdDialog;
      done();
    });
  });
  it('should do not create new dialog, but return existed', function(done) {
    models.Dialog.createNewDialog(twoUsersIds, function(err, createdDialog) {
      if(err) {
        return done(err);
      }

      createdDialog.unreadCounters.should.have.length(2);
      createdDialog.unreadCounters.forEach(function(unread) {
        unread.count.should.equal(1);
      });
      createdDialog.users.should.have.length(2);
      createdDialog.id.should.be.equal(newCreatedDialog.id);
      done();
    });
  });

  it('should send message to dialog from user1 to user2', function(done) {
    models.Dialog.newMessage(newCreatedDialog.id, {from: twoUsersIds[0], text: 'first message'}, function(err, addedMessage) {
      if(err) {
        return done(err);
      }

      addedMessage.should.have.properties({text: 'first message', status: 0});

      models.Dialog.findById(newCreatedDialog.id).exec(function(err, foundDialog) {
        if(err) {
          throw err;
        }

        foundDialog.messages.should.have.length(2);
        foundDialog.unreadCounters.should.have.length(2);
        _.reject(foundDialog.unreadCounters, function(unread) {
          return unread.user_id.toString() == twoUsersIds[0];
        })[0].count.should.equal(2);
        done();
      });
    });
  });

  it('should user2 read the message', function(done) {
    models.Dialog.findById(newCreatedDialog.id).exec(function(err, foundDialog) {
      if(err) {
        return done(err);
      }

      models.Dialog.markMessagesAsRead(_.pluck(foundDialog.messages, 'id'), foundDialog.id, twoUsersIds[1], function(err, updatedDialogs) {
        if(err) {
          return done(err);
        }

        updatedDialogs.messages.id(_.pluck(foundDialog.messages, 'id')[0]).status.should.be.equal(1);
        done();
      });
    });
  });
});

describe('Adding new user to dialog', function() {
  var addedUserId;
  before(function() {
    addedUserId = _.reject(createdUsers, function(user) {return twoUsersIds.indexOf(user.id) != -1;})[0].id;
  });
  it('should add new user to dialog', function(done) {
    models.Dialog.addUserToDialog(addedUserId, newCreatedDialog.id, function(err, updatedDialog) {
      if(err) {
        return done(err);
      }

      updatedDialog.users.should.have.property('length', 3);
      updatedDialog.users.indexOf(addedUserId).should.not.equal(-1);
      done();
    });
  });

  it('should new user get new dialog', function(done) {
    models.Dialog.getUserDialogs(addedUserId, function(err, foundDialogs) {
      if(err) {
        return done(err);
      }

      foundDialogs.should.have.property('length', 1);
      foundDialogs[0].users.length.should.equal(3);
      done();
    });
  });

  it('should send message to dialog from user3 to user1 and user2', function(done) {
    models.Dialog.newMessage(newCreatedDialog.id, {from: addedUserId, text: 'message from user3 to user1 and user2'}, function(err, addedMessage) {
      if(err) {
        return done(err);
      }

      addedMessage.should.have.properties({text: 'message from user3 to user1 and user2', status: 0});
      models.Dialog.findById(newCreatedDialog.id).exec(function(err, foundDialog) {
        if(err) {
          throw err;
        }

        foundDialog.messages.should.have.length(4);
        var usersMessages = _(foundDialog.messages).map(function(message) {return message.toObject();}).reject({messageType: 'service'}).value();
        usersMessages[0].should.have.properties({text: 'first message', status: 1});
        usersMessages[1].should.have.properties({text: 'message from user3 to user1 and user2', status: 0});
        _.reject(foundDialog.unreadCounters, {user_id: addedUserId})[0].count.should.equal(3);
        done();
      });
    });
  });

  //it('should user1 and user2 have unread count=1', function(done) {
  //  var promises = [];
  //  twoUsersIds.forEach(function(user_id) {
  //    var defer = Q.defer();
  //    models.Dialog.getUserDialogs(user_id, function(err, foundDialogs) {
  //      if(err) {
  //        return defer.reject(err);
  //      }
  //
  //      foundDialogs.length.should.equal(1);
  //
  //      _.find(_.find(foundDialogs, function(dialog) {
  //        return dialog.id == newCreatedDialog.id;
  //      }).unreadCounters, function(unread) {
  //        return unread.user_id.toString() == user_id.toString();
  //      }).count.should.equal(2);
  //      defer.resolve();
  //    });
  //    promises.push(defer.promise);
  //  });
  //  Q.all(promises).then(function() {
  //    done();
  //  }).catch(function(err) {
  //    done(err);
  //  });
  //});

});
