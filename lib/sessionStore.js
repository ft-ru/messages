
var config = require('./config'),
    session = require('express-session'),
    MongoStore = require('connect-mongo')(session);

module.exports = new MongoStore({
    url: config.get('mongoose:url'),
    autoReconnect: true,
    defaultExpirationTime: 1000 * 60 * 60 * 24 * 2 // 48 hours
  }, function() {});
