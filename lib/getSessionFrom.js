
var signature = require("cookie-signature"),
    cookie = require('cookie'),
    sessionStore = require('./sessionStore'),
    config = require('./config');

exports.cookies = function getSessionFromCookies(cookies, callback) {
  if(!cookies) {
    return callback(new Error('no cookies!'));
  }
  var parsedCookies = cookie.parse(cookies);
  if(!parsedCookies['connect.sid']) {
    return callback(new Error('no connect.sid cookie!'));
  }
  var real_sid = parsedCookies['connect.sid'].replace(config.get('cookiePrefix'), '');
  real_sid = signature.unsign(real_sid, config.get('sessionSecret'));
  sessionStore.get(real_sid, callback);
};

exports.session = function getSessionFromSession(session, callback) {
  if(!session) {
    return callback(new Error('no session!'));
  }
  var real_sid = session.replace(config.get('cookiePrefix'), '');
  real_sid = signature.unsign(real_sid, config.get('sessionSecret'));
  sessionStore.get(real_sid, callback);
};
