
var fs = require('fs'),
    path = require('path'),
    imagesFolderPath = path.resolve(__dirname, '..', 'public/images');

module.exports = {
  soc: {
    facebook: fs.readFileSync(path.join(imagesFolderPath, 'icon_facebook.svg')),
    twitter: fs.readFileSync(path.join(imagesFolderPath, 'icon_twitter.svg')),
    vk: fs.readFileSync(path.join(imagesFolderPath, 'icon_vk.svg'))
  }
};
