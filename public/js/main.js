

$(function(){

  if($('#uLoginAuth').length) {
    uLogin.customInit('uLoginAuth');
  }

  $(document).on('click', '.tabs_buttons .tab_btn', function() {
    $(this).addClass('active').siblings('.tab_btn').removeClass('active');
    $('#' + $(this).data('toggle')).addClass('active').siblings('.tab_content').removeClass('active');
  });

});

var socialAuth = function(token) {
  $.getJSON(
      "//ulogin.ru/token.php?host=" + encodeURIComponent(location.toString()) + "&token=" + token + "&callback=?",
      function(data) {
        data = $.parseJSON(data.toString());
        if(!data.error) {
          var obj = {
            name: data.first_name,
            surname: data.last_name,
            phone: data.phone,
            email: data.email,
            photo: {
              original: data.photo_big,
              preview: data.photo
            },
            social: {
              socialId: data.uid,
              socialNetwork: data.network,
              identity: data.identity
            },
            rawSocialData: data
          };
          $.post('/account/auth', obj, function(data) {
            console.log(data);
            if(data.status == 200) {
              document.location.reload();
            } else {
              notyError();
            }
          }).fail(function() {
            notyError();
          });
        }
      }
  );
};
