
var MessageItemView = Marionette.ItemView.extend({
  className: 'message',
  template: jade.templates['messageItem'],
  initialize: function(options) {
    var view = this;
    var userFrom = _.find(options.dialogModel.get('users'), function(user) {return user.id == view.model.get('from');}),
        //title = userFrom.fullName,
        originalTemplate = this.template,
        templateOptions = _.extend(this.model.toJSON(), {
          user: userFrom
        });

    this.template = _.bind(originalTemplate, this, templateOptions);
  },
  events: {
  }
});
