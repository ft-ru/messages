
var MessagesCompositeView = Marionette.CompositeView.extend({
  template: jade.templates['messagesCollection'],
  getChildView: function(item) {
    if(item.get('messageType') == 'service') {
      return MessageServiceItemView;
    } else {
      return MessageItemView;
    }
  },
  childViewContainer: '.messages_list',
  initialize: function(/*options*/) {
    this.newMessage = new MessageModel();
  },
  childViewOptions: function() {
    return {
      dialogModel: this.model
    };
  },
  ui: {
    textarea: '.message_input_wrp .textarea',
    addToDialogSelect: '[name="add_to_dialog"]',
    addAttachmentsArea: '.add_attachments_area'
  },
  events: {
    'click .add_to_dialog': function() {
      var view = this,
          user_id = view.ui.addToDialogSelect.val();

      socket.emit('dialog.addUser', {user_id: user_id, dialog_id: this.model.id}, function(err, updatedDialog) {
        console.log('dialog.addUser', err, updatedDialog);
        var dialog = dialogsCollection.findWhere({id: updatedDialog.id});

        dialog.updateFromServer(function() {
          var addedUser = _.findWhere(dialog.get('users'), {id: user_id});
          view.ui.addToDialogSelect.select2('data', null);
          notySuccess('Пользователь ' + addedUser.fullName + ' добавлен в диалог');
        });
      });
    },
    'keydown @ui.textarea': function(e) {
      var view = this,
          $textarea = $(e.target);

      if((e.keyCode == 13 && !e.shiftKey && !e.ctrlKey)) {
        this.newMessage.set('text', $textarea.html());
        socket.emit('dialogs.newMessage', {
          text: this.newMessage.get('text'),
          attachments: this.newMessage.get('attachments'),
          dialog_id: this.model.id
        }, function(err/*, addedMessage*/) {
          if(err) {
            return notyError();
          }

          $textarea.empty();
          view.ui.addAttachmentsArea.find('.add_file_link input[name^="attachments"]').remove();
          view.ui.addAttachmentsArea.find('.files_list .file').remove();
          view.newMessage.set('attachments', []);
          view.newMessage.set('text', '');
        });
      } else {
        this.newMessage.set('text', $textarea.html());
      }
    },
    'change .add_file_link .file_input': function(e) {
      var view = this,
          $this = $(e.target),
          $files_list = $this.closest('.add_attachments_area').find('.files_list.attachments'),
          files = $this[0].files,
          $lastAttachment = $this.siblings('input[name^="attachments"]:last'),
          lastAttachmentNumber = $lastAttachment.length ? ~~$lastAttachment.attr('name').replace(/\D+/g, '') || 0 : 0;

      sendFiles(files, {url: '/upload/attachments'}, function(err, files) {
        files.forEach(function(file, index) {
          if(_.findWhere(view.newMessage.get('attachments'), {url: file.url})) {
            return;
          }
          $files_list.append('<div class="file" data-url="'+ file.url +'">'+ file.originalFileName +'<div class="remove_icon"></div></div>');
          var input = '<input type="hidden" name="attachments['+ (lastAttachmentNumber + index + 1) +']" value="'+ file.url +'">';
          if($lastAttachment.length) {
            $lastAttachment.after(input);
          } else {
            $this.after(input);
          }
          view.newMessage.get('attachments').push({url: file.url, name: file.originalFileName});
          $this.replaceWith($this.val('').clone(true));
        });
      });
    }
  },
  onRender: function() {
    var view = this;
    this.$el.find('.select2_ajax').select2({
      placeholder: "Введите имя пользователя",
      minimumInputLength: 1,
      multiple: true,
      width: 240,
      ajax: {
        url: "/users",
        dataType: 'json',
        quietMillis: 250,
        data: function(term/*, page*/) {
          return {
            q: term
          };
        },
        results: function(data/*, page*/) {
          var alreadyInDialog = _(view.model.get('users')).pluck('id').value();
          var withoutMe = _.reject(data.users, function(user) {return alreadyInDialog.indexOf(user.id) != -1;});
          return {results: withoutMe};
        },
        cache: true
      },
      formatResult: repoFormatResult,
      formatSelection: repoFormatSelection
    });
  }
});
