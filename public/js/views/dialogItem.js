
var DialogItemView = Marionette.ItemView.extend({
  className: function() {
    var className = 'dialog';
    if(typeof this.model.get('unread') === 'undefined') {
      return className;
    }
    if(this.model.get('unread') && this.model.get('unread') > 0) {
      className = className + ' active';
    }
    return className;
  },
  template: jade.templates['dialogItem'],
  initialize: function() {
    var originalTemplate = this.template,
        users = this.model.get('users'),
        isMulti = users.length > 2,
        companions = _.reject(users, function(user) {return user.id == socket.session.user.id}),
        title = isMulti ? (this.model.get('title') || 'Диалог') : companions[0].fullName,
        options = _.extend(this.model.toJSON(), {
          isMulti: isMulti,
          title: title,
          companions: companions,
          imgUsersLength: companions.length >= 4 ? 4 : companions.length,
          usersImages: _(companions).sample(4).pluck('photo').pluck('preview').value()
        });

    this.template = _.bind(originalTemplate, this, options);

    this.listenTo(this.model, 'change', function() {
      var users = this.model.get('users'),
          isMulti = users.length > 2,
          companions = _.reject(users, function(user) {return user.id == socket.session.user.id}),
          title = isMulti ? (this.model.get('title') || 'Диалог') : companions[0].fullName,
          options = _.extend(this.model.toJSON(), {
            isMulti: isMulti,
            title: title,
            companions: companions,
            imgUsersLength: companions.length >= 4 ? 4 : companions.length,
            usersImages: _(companions).sample(4).pluck('photo').pluck('preview').value()
          });

      this.template = _.bind(originalTemplate, this, options);
      this.render();
    }, this);

    this.model.on('dialog:open', function() {
      this.triggerMethod('dialog:open', this.model);
    }, this);
  },
  onRender: function() {
    if(typeof this.model.get('unread') === 'undefined') {
      return;
    }
    if(this.model.get('unread') < 1) {
      this.$el.removeClass('active');
    } else {
      this.$el.addClass('active');
    }
  },
  events: {
    'click': function() {
      this.triggerMethod('dialog:open', this.model);
    }
  }
});
