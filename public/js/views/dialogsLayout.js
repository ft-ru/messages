
var DialogsLayoutView = Marionette.LayoutView.extend({
  template: jade.templates['dialogsContainer'],
  regions: {
    dialogs: '.dialogs_list'
  },
  el: '#messages',
  childEvents: {
    'dialog:open': function(view, dialogModel) {
      console.log('dialog:open', dialogModel);
      this.addDialogTab(dialogModel);
    }
  },
  initialize: function() {
    this.$tabsMenu = $('.news_messages_wrp .tabs_buttons');
    this.$tabsContents = $('.news_messages_wrp .tabs_content');
  },
  addDialogTab: function(dialogModel) {
    var isMulti = dialogModel.get('users').length > 2,
        companion = _.find(dialogModel.get('users'), function(user) {return user.id != socket.session.user.id}),
        title = isMulti ? (dialogModel.get('title') || 'Диалог') : companion.fullName,
        dialog_id = 'dialog_'+ dialogModel.get('id'),
        $accountNotificationsCounter = $('.account_link .notifications');

    this.$tabsMenu.find('.tab_btn').removeClass('active');
    if(this.$tabsMenu.find('[data-toggle="'+ dialog_id +'"]').length) {
      this.$tabsMenu.find('[data-toggle="'+ dialog_id +'"]').addClass('active');
    } else {
      this.$tabsMenu.append('<div data-toggle="'+ dialog_id +'" class="tab_btn active">'+ title +'</div>');
    }

    this.$tabsContents.find('.tab_content').removeClass('active');
    if(this.$tabsContents.find('#' + dialog_id).length) {
      this.$tabsContents.find('#' + dialog_id).addClass('active');
    } else {
      this.$tabsContents.append('<div id="'+ dialog_id +'" class="tab_content active"></div>');
    }

    socket.emit('dialog:read', dialogModel.get('id'), function(err, foundDialog) {
      if(err) {
        console.log('dialog:read', err);
        return notyError();
      }
      var dialog = dialogsCollection.findWhere({id: foundDialog.id});
      // TODO: maybe set all properties
      dialog.messages.reset(foundDialog.messages);
      new MessagesCompositeView({model: dialog, collection: dialog.messages, el: '#' + dialog_id}).render();

      var readMessages = _(dialog.messages.toJSON())
          .filter(function(message) {
            return message.status == 0 && message.from != socket.session.user.id;
          })
          .pluck('id')
          .value();

      if(readMessages.length) {
        if($accountNotificationsCounter.text()) {
          var newNotificationsCounterValue = parseInt($accountNotificationsCounter.text(), 10) - readMessages.length;
          $accountNotificationsCounter.text(newNotificationsCounterValue);
          if(newNotificationsCounterValue < 1) {
            $accountNotificationsCounter.hide();
          } else {
            $accountNotificationsCounter.show();
          }
        }
        socket.emit('messages.markAsRead', readMessages, dialog.id, function(err/*, updatedDialog*/) {
          if(err) {
            console.log('socket.emit("messages.markAsRead"', err);
            //notyError();
          }
          //console.log('messages.markAsRead', err, updatedDialog);
        });
      }
    });
  }
});

var dialogsLayoutView = new DialogsLayoutView();
