
var socket = io(document.location.protocol + '//' + document.location.hostname + ':3401');

socket.on('connect', function() {
  console.log('socket connected');
  socket.emit('dialogs:read', function(err, dialogs) {
    if(err) {
      console.log('socket.emit("dialogs:read"', err);
      return notyError();
    }

    dialogs.forEach(function(dialog) {
      dialogsCollection.add(dialog);
    });
  });
});
socket.on('disconnect', function() {
  console.log('socket disconnected', arguments);
});
socket.on('error', function(err) {
  console.log('socket error', err);
});

socket.on('auth', function(session) {
  socket.session = session;
});

socket.on('dialogs.newMessage', function(data) {
  console.log('dialogs.newMessage', data);
  var dialog = dialogsCollection.findWhere({id: data.dialog_id});
  dialog.messages.add(data.message);
  // FIXME: maybe fetch from server?
  //dialogsCollection.findWhere({id: '54f46ea5b5c080842ea0738b'}).fetch({success: function() {console.log('success', arguments);}, attrs: '54f46ea5b5c080842ea0738b'});
  dialog.set('lastMessage', data.message);
  if(data.message.from != socket.session.user.id) {
    dialog.set('unread', dialog.get('unread') + 1);
    if($('#dialog_' + data.dialog_id).is('.active')) {
      socket.emit('messages.markAsRead', [data.message.id], dialog.id, function(err/*, updatedDialog*/) {
        if(err) {
          console.log('socket.emit("messages.markAsRead"', err);
          notyError();
        }
      });
    }
  }
  dialogsCollection.sort();
});

socket.on('dialogs:create', function() {
  console.log('dialogs:create');
  socket.emit('dialogs:read', function(err, dialogs) {
    if(err) {
      return notyError();
    }

    dialogs.forEach(function(dialog) {
      dialogsCollection.add(dialog);
    });
  });
});

$(function() {

  dialogsLayoutView.render();
  dialogsLayoutView.getRegion('dialogs').show(dialogsCompositeView);

  $('.select2_ajax').select2({
    placeholder: "Введите имя пользователя",
    minimumInputLength: 1,
    multiple: true,
    width: 240,
    ajax: {
      url: "/users",
      dataType: 'json',
      quietMillis: 250,
      data: function(term/*, page*/) {
        return {
          q: term
        };
      },
      results: function(data/*, page*/) {
        return {results: data.users};
      },
      cache: true
    },
    formatResult: repoFormatResult,
    formatSelection: repoFormatSelection
  });

  $(document).on('click', '.create_new_dialog', function() {
    socket.emit('dialogs:create', {user_id: $('[name="create_dialog"]').val()}, function(err, createdDialog) {
      dialogsCollection.add(createdDialog);
    });
  });

  $(document).on('click', '.upload_button', function(e) {
    if(!$(e.target).is('input')) {
      $(this).find('input').trigger('click');
    }
  });

  $(document).on('change', '.edit_photo_wrp input[type="file"]', function() {
    var $this = $(this),
        $image_col = $this.closest('.image_col');

    sendFiles($this[0].files, {url: '/upload/userpic'}, function(err, files) {
      if(err) {
        return notyError();
      }

      var photo = files[0];
      photo.original = photo.url;
      $image_col.find('.image').attr('src', photo.preview);
      $this.replaceWith($this.clone().val(''));
      $.post('/account/edit', {photo: photo}).done(function(data) {
        if(data.status == 200) {
          notySuccess('Ваша фотография обновлена!');
        } else {
          notyError();
        }
      }).fail(function() {
        notyError();
      });
    });
  });

  Backbone.history.start();

  if(/messageTo/.test(document.location.hash)) {
    socket.emit('dialogs:create', {user_id: document.location.hash.substr(1).split('=')[1]}, function(err, createdDialog) {
      dialogsCollection.add(createdDialog);
      dialogsCollection.findWhere({id: createdDialog.id}).trigger('dialog:open');
      document.location.hash = '';
      $('html, body').animate({scrollTop: $('#dialog_' + createdDialog.id).offset().top});
    });
  }
});

function repoFormatResult(user) {
  return jade.templates['findUserAutoCompleteItem'](user);
}

function repoFormatSelection(user) {
  return user.fullName;
}

function nestCollection(model, attributeName, nestedCollection) {
  //setup nested references
  for (var i = 0; i < nestedCollection.length; i++) {
    model.attributes[attributeName][i] = nestedCollection.at(i).attributes;
  }
  //create empty arrays if none

  nestedCollection.bind('add', function (initiative) {
    if (!model.get(attributeName)) {
      model.attributes[attributeName] = [];
    }
    model.get(attributeName).push(initiative.attributes);
  });

  nestedCollection.bind('remove', function (initiative) {
    var updateObj = {};
    updateObj[attributeName] = _.without(model.get(attributeName), initiative.attributes);
    model.set(updateObj);
  });
  return nestedCollection;
}

