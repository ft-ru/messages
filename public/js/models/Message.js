
var MessageModel = Backbone.Model.extend({
  defaults: {
    //from: {type: Schema.Types.ObjectId, ref: 'User'},
    //text: String,
    createdAt: Date.now(),
    status: 0, // 0 - not read, 1 - read
    attachments: []
  }
});
