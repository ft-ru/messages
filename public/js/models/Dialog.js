
var DialogModel = Backbone.Model.extend({
  defaults: {
    users: [],
    //messages: [],
    //title: {type: String},
    //lastMessage: messagesSchemaObject,
    unreadCounters: []
  },
  initialize: function() {
    this.messages = nestCollection(this, 'messages', new MessagesCollection(this.get('messages')));
  },
  url: '/dialog',
  updateFromServer: function(callback) {
    var model = this;
    socket.emit('dialog:read', model.id, function(err, updatedDialog) {
      if(err) {
        return callback(err);
      }
      _.each(updatedDialog, function(val, key) {
        model.set(key, val);
      });
      callback(null, model);
    });
  }
});
