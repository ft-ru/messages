
var DialogsCollection = Backbone.Collection.extend({
  model: DialogModel,
  url: '/dialogs',
  comparator: function(a, b) {
    if(a.get('lastMessage').createdAt > b.get('lastMessage').createdAt) {
      return 0;
    } else {
      return 1;
    }
  }
});

var dialogsCollection = new DialogsCollection([]);

